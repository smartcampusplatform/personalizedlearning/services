<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Performance::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'mata_kuliah' => $faker->name,
        'progress' => $faker->numberBetween($min = 0, $max = 100),
        'nilai_quiz' => $faker->numberBetween($min = 0, $max = 100),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
    ];
});
