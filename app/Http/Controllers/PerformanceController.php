<?php

namespace App\Http\Controllers;

use App\Performance;
use Illuminate\Http\Request;

class PerformanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Performance::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(request $request)
    {
        //
        $performance = new Performance;
        $performance->mata_kuliah = $request->mata_kuliah;
        $performance->progress = $request->progress;
        $performance->nilai_quiz = $request->nilai_quiz;
        $performance->feedback = $request->feedback;
        $performance->save();

        return "Data berhasil masuk";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Performance  $performance
     * @return \Illuminate\Http\Response
     */
    public function show(Performance $performance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Performance  $performance
     * @return \Illuminate\Http\Response
     */
    public function edit(Performance $performance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Performance  $performance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $mata_kuliah = $request->mata_kuliah;
        $progress = $request->progress;
        $nilai_quiz = $request->nilai_quiz;
        $feedback = $request->feedback;

        $performance = Performance::find($id);
        $performance->mata_kuliah = $mata_kuliah;
        $performance->progress = $progress;
        $performance->nilai_quiz = $nilai_quiz;
        $performance->feedback = $feedback;
        $performance->save();

        return "Data berhasil di-update";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Performance  $performance
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $performance = Performance::find($id);
        $performance->delete();

        return "Data berhasil di-hapus";
    }
}
