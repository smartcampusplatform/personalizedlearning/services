<div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="index.html">
                    <i class="material-icons">dashboard</i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="pages/typography.html">
                    <i class="material-icons">linear_scale</i>
                    <span>Learning Path</span>
                </a>
            </li>
            <li>
                <a href="pages/typography.html">
                    <i class="material-icons">code</i>
                    <span>Courses</span>
                </a>
            </li>
            <li>
                <a href="pages/typography.html">
                    <i class="material-icons">archive</i>
                    <span>Activity Record</span>
                </a>
            </li>
            <li>
                <a href="pages/typography.html">
                    <i class="material-icons">assignment</i>
                    <span>Assignment</span>
                </a>
            </li>
            <li>
                <a href="pages/typography.html">
                    <i class="material-icons">book</i>
                    <span>Library Reference</span>
                </a>
            </li>
            <li>
                <a href="pages/typography.html">
                    <i class="material-icons">list</i>
                    <span>Quiz</span>
                </a>
            </li>
            <li>
                <a href="pages/typography.html">
                    <i class="material-icons">assessment</i>
                    <span>Performance Report</span>
                </a>
            </li>
            <li>
                <a href="pages/typography.html">
                    <i class="material-icons">text_fields</i>
                    <span>Typography</span>
                </a>
            </li>
            <li>
                <a href="pages/helper-classes.html">
                    <i class="material-icons">layers</i>
                    <span>Helper Classes</span>
                </a>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">widgets</i>
                    <span>Widgets</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <span>Cards</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="pages/widgets/cards/basic.html">Basic</a>
                            </li>
                            <li>
                                <a href="pages/widgets/cards/colored.html">Colored</a>
                            </li>
                            <li>
                                <a href="pages/widgets/cards/no-header.html">No Header</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <span>Infobox</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="base/pages/widgets/infobox/infobox-1.html">Infobox-1</a>
                            </li>
                            <li>
                                <a href="base/pages/widgets/infobox/infobox-2.html">Infobox-2</a>
                            </li>
                            <li>
                                <a href="base/pages/widgets/infobox/infobox-3.html">Infobox-3</a>
                            </li>
                            <li>
                                <a href="base/pages/widgets/infobox/infobox-4.html">Infobox-4</a>
                            </li>
                            <li>
                                <a href="base/pages/widgets/infobox/infobox-5.html">Infobox-5</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">swap_calls</i>
                    <span>User Interface (UI)</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="base/pages/ui/alerts.html">Alerts</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/animations.html">Animations</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/badges.html">Badges</a>
                    </li>

                    <li>
                        <a href="base/base/pages/ui/breadcrumbs.html">Breadcrumbs</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/buttons.html">Buttons</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/collapse.html">Collapse</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/colors.html">Colors</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/dialogs.html">Dialogs</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/icons.html">Icons</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/labels.html">Labels</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/list-group.html">List Group</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/media-object.html">Media Object</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/modals.html">Modals</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/notifications.html">Notifications</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/pagination.html">Pagination</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/preloaders.html">Preloaders</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/progressbars.html">Progress Bars</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/range-sliders.html">Range Sliders</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/sortable-nestable.html">Sortable & Nestable</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/tabs.html">Tabs</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/thumbnails.html">Thumbnails</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/tooltips-popovers.html">Tooltips & Popovers</a>
                    </li>
                    <li>
                        <a href="base/pages/ui/waves.html">Waves</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">assignment</i>
                    <span>Forms</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="base/pages/forms/basic-form-elements.html">Basic Form Elements</a>
                    </li>
                    <li>
                        <a href="base/pages/forms/advanced-form-elements.html">Advanced Form Elements</a>
                    </li>
                    <li>
                        <a href="base/pages/forms/form-examples.html">Form Examples</a>
                    </li>
                    <li>
                        <a href="base/pages/forms/form-validation.html">Form Validation</a>
                    </li>
                    <li>
                        <a href="base/pages/forms/form-wizard.html">Form Wizard</a>
                    </li>
                    <li>
                        <a href="base/pages/forms/editors.html">Editors</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">view_list</i>
                    <span>Tables</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="base/pages/tables/normal-tables.html">Normal Tables</a>
                    </li>
                    <li>
                        <a href="base/pages/tables/jquery-datatable.html">Jquery Datatables</a>
                    </li>
                    <li>
                        <a href="base/pages/tables/editable-table.html">Editable Tables</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">perm_media</i>
                    <span>Medias</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="base/pages/medias/image-gallery.html">Image Gallery</a>
                    </li>
                    <li>
                        <a href="base/pages/medias/carousel.html">Carousel</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">pie_chart</i>
                    <span>Charts</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="base/pages/charts/morris.html">Morris</a>
                    </li>
                    <li>
                        <a href="base/pages/charts/flot.html">Flot</a>
                    </li>
                    <li>
                        <a href="base/pages/charts/chartjs.html">ChartJS</a>
                    </li>
                    <li>
                        <a href="base/pages/charts/sparkline.html">Sparkline</a>
                    </li>
                    <li>
                        <a href="base/pages/charts/jquery-knob.html">Jquery Knob</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">content_copy</i>
                    <span>Example Pages</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="base/pages/examples/profile.html">Profile</a>
                    </li>
                    <li>
                        <a href="base/pages/examples/sign-in.html">Sign In</a>
                    </li>
                    <li>
                        <a href="base/pages/examples/sign-up.html">Sign Up</a>
                    </li>
                    <li>
                        <a href="base/pages/examples/forgot-password.html">Forgot Password</a>
                    </li>
                    <li>
                        <a href="base/pages/examples/blank.html">Blank Page</a>
                    </li>
                    <li>
                        <a href="base/pages/examples/404.html">404 - Not Found</a>
                    </li>
                    <li>
                        <a href="base/pages/examples/500.html">500 - Server Error</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">map</i>
                    <span>Maps</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="base/pages/maps/google.html">Google Map</a>
                    </li>
                    <li>
                        <a href="base/pages/maps/yandex.html">YandexMap</a>
                    </li>
                    <li>
                        <a href="base/pages/maps/jvectormap.html">jVectorMap</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">trending_down</i>
                    <span>Multi Level Menu</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="javascript:void(0);">
                            <span>Menu Item</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <span>Menu Item - 2</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <span>Level - 2</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="javascript:void(0);">
                                    <span>Menu Item</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>Level - 3</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <span>Level - 4</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="base/pages/changelogs.html">
                    <i class="material-icons">update</i>
                    <span>Changelogs</span>
                </a>
            </li>
            <li class="header">LABELS</li>
            <li>
                <a href="javascript:void(0);">
                    <i class="material-icons col-red">donut_large</i>
                    <span>Important</span>
                </a>
            </li>
            <li>
                <a href="javascript:void(0);">
                    <i class="material-icons col-amber">donut_large</i>
                    <span>Warning</span>
                </a>
            </li>
            <li>
                <a href="javascript:void(0);">
                    <i class="material-icons col-light-blue">donut_large</i>
                    <span>Information</span>
                </a>
            </li>
        </ul>
    </div>